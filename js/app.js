// dropdown

let btnMenu = document.querySelector('.menu-header-mob');
let ul = document.querySelector('ul.menu-header');
btnMenu.addEventListener('click', () => {
    ul.style.display === 'block'
    ? ul.style.display = 'none'
    : ul.style.display = 'block';
});

// cards

let cards = document.querySelectorAll('.card');
cards.forEach(function (item) {
    item.addEventListener('click', function () {
        item.classList.toggle('is-flipped');
    });
});

// slider

let btnNext = document.querySelector('.next');
let btnPrev = document.querySelector('.prev');
let list = document.querySelector('.list');

btnNext.addEventListener('click', () => {
    let margin = parseInt(list.style.marginLeft);
    if (margin > -1280) {
        let newMargin = margin - 640;
        list.style.marginLeft = newMargin + 'px';
    }
});
btnPrev.addEventListener('click', () => {
    let margin = parseInt(list.style.marginLeft);
    if (margin < 0) {
        let newMargin = margin + 640;
        list.style.marginLeft = newMargin + 'px';
    }
});

// validation

let btnSubmit = document.querySelector('button[type="submit"]');
let input = document.querySelector('input[type="email"]');
btnSubmit.addEventListener('click', validationEmail);

function validationEmail(e) {
    e.preventDefault();
    let email = input.value;
    if (email.length > 5) {
        if (email.indexOf('@') != -1 ){
            if (email.indexOf('@') != 0 && email.indexOf('@') != email.length - 1) {
                if (email.length < 256) {
                    console.log('OK');
                } else {
                    alert('too long');
                }
            } else {
                alert('error');
            }
        } else {
            alert('enter email with @');
        }
    } else {
        alert('enter more than 5 characters');
    }
}

// filter

let gallery = document.querySelectorAll('.grey-layer');
let btnFilter = document.getElementById('all');
let btnsFilter = document.querySelectorAll('.filter')


btnsFilter.forEach(function (item) {
    item.addEventListener('click', () => {
        let lenghtGallary = gallery.length;
        for (var i = 0; i < lenghtGallary; i++) {
            gallery[i].style.display = 'block';
        }
        let filterName = item.id;
        for (var i = 0; i < lenghtGallary; i++) {
            if(!gallery[i].classList.contains(filterName)){
                gallery[i].style.display = 'none';
            }
        }
    });
});


btnFilter.addEventListener('click', () => {
    let lenghtGallary = gallery.length;
    for (var i = 0; i < lenghtGallary; i++) {
        gallery[i].style.display = 'block';
    }
});

// tabs


    let tabContent = document.querySelectorAll('.tabContent');
    let tab = document.querySelectorAll('.tab');
    let tabs = document.getElementById('tabs');
    hideTabsContent(1);

function hideTabsContent(a){
    for(var i = a; i < tabContent.length; i++){
        tabContent[i].classList.remove('show');
        tabContent[i].classList.add('hide');
        tab[i].classList.remove('icon-active');
    }
}

tabs.addEventListener('click', (e) => {
    let target = e.target;
    if(target.className === 'tab'){
        for(var i = 0; i < tab.length; i++){
            if(target === tab[i]){
                showTabsContent(i);
                break;
            }
        }
    }
    if(target.classList.contains('tab-child')){
        for(var i = 0; i < tab.length; i++){
            if(target.parentElement === tab[i]){
                showTabsContent(i);
                break;
            }
        }
    }
})

function showTabsContent(b){
    if(tabContent[b].classList.contains('hide')){
        hideTabsContent(0);
        tab[b].classList.add('icon-active');
        tabContent[b].classList.remove('hide');
        tabContent[b].classList.add('show');
    }
}

// map

let btnMap = document.querySelector('.map-cover > .white-title');
btnMap.addEventListener('click', () => {
    let mapCover = document.querySelector('.map-cover');
    mapCover.style.display = "none"

})





